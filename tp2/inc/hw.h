#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT      27  // ASCII para la tecla Esc
#define FICHA  49  // ASCII para la tecla 1
#define BOTON_TE  50  // ASCII para la tecla 2
#define BOTON_CAFE  51  // ASCII para la tecla 3

/*==================[typedef]================================================*/
typedef enum {
    ESPERANDO_FICHA,
    SELECCIONAR,
    SIRVIENDO_TE,
    SIRVIENDO_CAFE,
    INDICADOR_SONORO
} MAQUINA;
/*==================[external data declaration]==============================*/


/*==================[external functions declaration]=========================*/

// Funciones que configuran la consola de Linux como interfaz de I/O
void hw_Init(void);
void hw_DeInit(void);

// Funciones basicas para leer que entrada esta activa y pausar la ejecucion
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);

void hw_MostrarOpciones(void);
void hw_ServirTe(void);
void hw_ServirCafe(void);
void hw_ActivarIndicadorSonoro(void);
void hw_DesactivarIndicadorSonoro(void);
uint8_t toggleled(uint8_t);
void ciclo_maquina(void);
void evTick1seg(void);
void ficha_insertada(void);
void evboton_te_presionado(void);
void evboton_cafe_presionado(void);
void hw_Dejar_de_servir_Cafe(void);
void hw_Dejar_de_servir_Te(void);
void evTick100ms(void);
void evTick500ms(void);
void hw_devolverficha(void);




/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
