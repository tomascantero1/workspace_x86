/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include <stdbool.h>
#include <stdio.h>
/*==================[macros and definitions]=================================*/

#define TIEMPO_ESPERA  10            //tiempo en segundos reducido para mejor visualizacion del funcionamiento
#define TIEMPO_TE 3               //tiempo en segundos reducido para mejor visualizacion del funcionamiento
#define TIEMPO_CAFE 6              //tiempo en segundos reducido para mejor visualizacion del funcionamiento
#define TIEMPO_SONIDO 2          //tiempo en segundos

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
bool evficha;
bool evboton_te;
bool evboton_cafe;
bool evTick1seg_raised;
bool evTick100ms_raised;
bool evTick500ms_raised;
uint8_t count_seg,led,led_estado;

MAQUINA state;
/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/
static void clearEvents(void)
{   
    evficha=0;
    evboton_te=0;
    evboton_cafe = 0;
    evTick1seg_raised = 0;
    evTick100ms_raised=0;
    evTick500ms_raised=0;
}

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input = 0;
    uint16_t cont_10ms = 0,cont_100ms=0,cont_500ms=0;

    hw_Init();

    

    while (input != EXIT) {
        input = hw_LeerEntrada();
        if(input==FICHA){
           ficha_insertada();   
        }
        
        if(input==BOTON_TE){
          evboton_te_presionado();
        }

        if(input==BOTON_CAFE){
          evboton_cafe_presionado(); 
        }
        
        // En un microcontrolador esto se implementaria en un handler de
        // interrupcion asociado a un timer
        cont_10ms++;
        if (cont_10ms == 100) {
            cont_10ms = 0;
            evTick1seg();
        }
        cont_100ms++;
        if (cont_100ms==10){
            cont_100ms =0;
            evTick100ms();  
        }

        cont_500ms++;
        if (cont_500ms==50){
            cont_500ms =0;
            evTick500ms();  
        }
        
        ciclo_maquina();
        hw_Pausems(10);
        
    }

    hw_DeInit();
    return 0;
}

void ciclo_maquina(void)
{
    switch (state) {
        case ESPERANDO_FICHA:
            if(evficha){
                hw_MostrarOpciones();
                state=SELECCIONAR;
            } 
            else if(evTick500ms_raised){
                led_estado=toggleled(led);
                led=led_estado;
            }
            break;

        case SELECCIONAR:
            if(evboton_te){
                hw_ServirTe();
                state=SIRVIENDO_TE;
            }
            else if(evboton_cafe){
                hw_ServirCafe();
                state=SIRVIENDO_CAFE;   
            }
            else if (evTick1seg_raised && (count_seg < TIEMPO_ESPERA)) {
                count_seg++;
             
            }
            else if (evTick1seg_raised && (count_seg == TIEMPO_ESPERA)) {
                count_seg=0;
                hw_devolverficha();
                state = ESPERANDO_FICHA;
            }
            break;

        case SIRVIENDO_TE:
            
            if(evTick1seg_raised && (count_seg < TIEMPO_TE)){
                count_seg++;
            }
            else if (evTick1seg_raised && (count_seg == TIEMPO_TE)) {
              count_seg=0;
              hw_ActivarIndicadorSonoro();
              state =INDICADOR_SONORO;
            }

            else if (evTick100ms_raised){
            led_estado=toggleled(led);
            led=led_estado;
            }
            break; 

        case SIRVIENDO_CAFE:
           if(evTick1seg_raised && (count_seg < TIEMPO_CAFE)){
                count_seg++;
            }
            else if (evTick1seg_raised && (count_seg == TIEMPO_CAFE)) {
              count_seg=0;
              hw_ActivarIndicadorSonoro();
              state =INDICADOR_SONORO;
            }

            else if (evTick100ms_raised){
            led_estado=toggleled(led);
             led=led_estado;
            }
            break; 

        case INDICADOR_SONORO:
            if (evTick1seg_raised && (count_seg < TIEMPO_SONIDO)) {
                count_seg++;
               
            }
            else if (evTick1seg_raised && (count_seg == TIEMPO_SONIDO)) {
            hw_DesactivarIndicadorSonoro();
            count_seg=0;
            state =ESPERANDO_FICHA;
            }
            break;

    }
    
clearEvents();
    
}

void ficha_insertada(void)
{
    evficha= 1;
}

void evboton_te_presionado(void)
{
    evboton_te = 1;
}

void evboton_cafe_presionado(void)
{
    evboton_cafe = 1;
}

void hw_MostrarOpciones(void)
{
    printf("SELECCIONAR:   2-TE   3-CAFE \n\n");
}

void evTick1seg(void)
{
    evTick1seg_raised = 1;
}

void evTick100ms(void)
{
    evTick100ms_raised = 1;
}
void evTick500ms(void)
{
    evTick500ms_raised = 1;
}
/*==================[end of file]============================================*/
