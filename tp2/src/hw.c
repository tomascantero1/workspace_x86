/*==================[inclusions]=============================================*/

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

void hw_Seleccionar(void)
{
    printf("Seleccionar:  2_Té  3_Café \n\n");
}

void hw_ServirTe(void)
{
    printf("Sirviendo Té \n\n");
}

void hw_Dejar_de_servir_Te(void)
{
    printf("Té Servido \n\n");
}
void hw_ServirCafe(void)
{
    printf("Sirviendo Café \n\n");
}

void hw_Dejar_de_servir_Cafe(void)
{
    printf("Café Servido \n\n");
}

void hw_ActivarIndicadorSonoro(void)
{
    printf("Indicador sonoro ENCENDIDO \n\n");
}

void hw_DesactivarIndicadorSonoro(void)
{
    printf("Indicador sonoro APAGADO \n\n");
}

void hw_devolverficha(void){
printf("DEVOLVER FICHA \n\n");
}

uint8_t toggleled(uint8_t led)
{
    if(!led){
       printf("LED ENCENDIDO \n\n");
       led=!led;
       return led;
    }
    else{
       printf("LED APAGADO\n\n");
       led=!led;
       return led;
    }
}


/*==================[end of file]============================================*/
