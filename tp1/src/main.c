/*==================[inclusions]=============================================*/
#include "main.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input, flag=0,tiempo=0;                 

    hw_Init();

    // Superloop hasta que se presione la tecla Esc
   while (input != EXIT) {
        input = hw_LeerEntrada();

        if (input == SENSOR_1) {
            hw_AbrirBarrera();
            flag=1;
        }

        if (input == SENSOR_2 && flag==1){           //Sensor 2 activado con barrera alta
            while(tiempo!=50){
            input = hw_LeerEntrada(); 
                if(input==SENSOR_1){
                    while(input!=SENSOR_2){
                      input = hw_LeerEntrada();
                      hw_Pausems(100);
                    }
                }
                if(input==SENSOR_2){
                   tiempo=0; 
                }
            hw_Pausems(100);
            tiempo++;
            } 
          
        hw_CerrarBarrera();  
        tiempo=0;
        flag=0;   
        }

        if(input == SENSOR_2 && flag==0){        //Sensor 2 activado con barrera baja 
           hw_Alarma();
        }
        
        hw_Pausems(100);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
