#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT      27  // ASCII para la tecla Esc 
#define SENSOR_1  49  // ASCII para la tecla 1 
#define SENSOR_2  50  // ASCII para la tecla 2 

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

void hw_Init(void);
void hw_DeInit(void);
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);
void hw_AbrirBarrera(void);
void hw_CerrarBarrera(void);
void hw_Alarma(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
